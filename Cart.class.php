<?php
class Cart
{
    protected $cartProducts = [];

    public function __construct($cartProducts = [])
    {
        $this->cartProducts = $cartProducts;
    }

    public function getcartProducts()
    {
        return $this->cartProducts;
    }

    public function add($name, $price, $quantity = 1): bool
    {
        try {
            if (!empty($this->cartProducts[$name])) {
                $quantity += intval($this->cartProducts[$name]['quantity']);
            }
            $total = round($price * $quantity, 2);
            $this->cartProducts[$name] = [
                'name' => $name,
                'price' => $price,
                'quantity' => $quantity,
                'total' => $total,
            ];
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    public function remove($name): bool
    {
        try {
            if (!empty($this->cartProducts[$name])) {
                unset($this->cartProducts[$name]);
                return true;
            }
        } catch (Exception $e) {
            return false;
        }
        return false;
    }

    public function grandTotal()
    {
        $grandTotal = array_sum(array_column($this->cartProducts, 'total'));
        return number_format(round($grandTotal, 2), 2);
    }

    public function totalQuantity()
    {
        $totalQuantity = array_sum(array_column($this->cartProducts, 'quantity'));
        return number_format(round($totalQuantity, 2), 2);
    }
}
