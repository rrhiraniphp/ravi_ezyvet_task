<?php

declare(strict_types=1);
error_reporting(E_ALL);
// session
session_start();
require_once("Cart.class.php");
require_once("Product.class.php");
// get all products
$products =  Product::getProducts();
// load cart products from the session
$cartProducts = $_SESSION['cart_products'] ?? [];
// cart operations
$cart = new Cart($cartProducts);
$cartProducts = $cart->getcartProducts();
$cartTotalQuantity = $cart->totalQuantity();
$cartGrandTotal = $cart->grandTotal();
if (!empty($_GET['action']) && !empty($_GET['product_name'])) {
    $cartAction = $_GET['action'];
    $productName = $_GET['product_name'];
    $acknowledged = false;
    if ($cartAction == 'add') {
        $key = array_search($productName, array_column($products, 'name'));
        $unitPrice = $products[$key]['price'] ?? 0;
        if (!empty($unitPrice)) {
            $acknowledged = $cart->add($productName, $unitPrice);
        }
    } else if ($cartAction == 'remove') {
        $acknowledged = $cart->remove($productName);
    } else {
        // error handling unknown action
    }
    if ($acknowledged)
        $_SESSION['cart_products'] = $cart->getcartProducts();
    //self redirection
    header("location:{$_SERVER['PHP_SELF']}");
}
?>

<html>

<head>
    <title>ezyVet Developers Practical Task</title>
</head>

<body>
    <section style="height: 35%;">
        <h2>Cart</h3>

            <?php if (empty($cartProducts)) { ?>
                Cart is empty. Please add some products below by clicking on Add button.
            <?php } else { ?>
                <table>
                    <tr>

                        <th style="text-align:left;">Name</th>
                        <th style="text-align:right;" width="15%">Price</th>
                        <th style="text-align:right;" width="15%">Quantity</th>
                        <th style="text-align:right;" width="15%">Total</th>
                        <th style="text-align:center;" width="10%">Action</th>
                    </tr>
                    <?php foreach ($cartProducts as $cartProduct) : ?>
                        <form method="post" action="index.php?action=remove&product_name=<?php echo $cartProduct['name'] ?>">
                            <tr>
                                <td style="text-align:left;"><?php echo $cartProduct['name'] ?? '' ?></td>
                                <td style="text-align:right;"><?php echo !empty($cartProduct['price']) ? number_format($cartProduct['price'], 2) : 0.0; ?></td>
                                <td style="text-align:right;"><?php echo $cartProduct['quantity'] ?? 0; ?></td>
                                <td style="text-align:right;"><?php echo !empty($cartProduct['total']) ? number_format($cartProduct['total'], 2) : 0.0; ?></td>
                                <td><input type="submit" value="Remove" /></td>
                            </tr>
                        </form>
                    <?php endforeach; ?>
                    <tr>

                        <td align="right" colspan="3"><strong><?php echo $cartTotalQuantity; ?></strong></td>

                        <td align="right"><strong><?php echo $cartGrandTotal ?></strong></td>
                        <td align="right"><strong>Grand Total<strong></td>
                        <td></td>
                    </tr>
                </table>
            <?php } ?>
    </section>
    <section>
        <h2>Products</h2>

        <table>
            <tr>
                <th style="text-align:left;" width="30%">Product Name</th>
                <th style="text-align:left;" width="30%">Price</th>
                <th style="text-align:left;" width="30%">Action</th>
            </tr>
            <?php foreach ($products as $product) { ?>
                <form method="post" action="index.php?action=add&product_name=<?php echo $product['name'] ?>">
                    <tr>
                        <td><?php echo $product['name'] ?></td>
                        <td><?php echo number_format($product['price'], 2) ?></td>
                        <td><input type="submit" value="Add" /></td>
                    </tr>
                </form>
            <?php } ?>
        </table>
    </section>